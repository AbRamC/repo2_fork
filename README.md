# GIT practicas#

Este repo contiene los archivos de la practica de GIT de Sistemas Informaticos DAW-B

### Autores ###
- Samuel E. Ramirez C.
- Abiezer J. Ramirez C.
- Pillar Garcia Rojas

### ¿para que es este repositorio? ###

* Resumen rápido
Este repositorio contiene dos ficheros Java empleados para la practica de GIT.
* Version
0.0.0.1a

### Algoritmo 1 ###
Opera con enteros.

#### Número Par o Impar #####
Retorna si el número ingresado es par o impar.
#### Divisores Número #####
Muestra todos los divisores de ese número.

### Algoritmo 2 ###
Se encarga de operar con cadenas de caractere para saber su longitud y el ***indice*** de cada una de sus letras en el abecedario.

#### Longitud Cadena ####
Retorna la longitud de la cadena.
#### Posición en el Abecedario ####
Retorna la posición en el abecedario de cada caracter que se le ha pasado al metodo.
