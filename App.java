import java.util.Scanner;
public class App {
    public static void main(String[] args) throws Exception {
       

        Scanner sc = new Scanner(System.in);

        System.out.println("Introduce un número:");
        int num = sc.nextInt();

        parImp(num);

        divisores(num);

        sc.close();
    }

    public static void parImp(int n){
        if(n%2 == 0){
            System.out.println("El número "+n+" es par ");
        }
        else{
            System.out.println("El número "+n+" es impar");
        }
    }

    public static void divisores(int n){

        System.out.println("Divisores de "+n+":");
        System.out.print("[");
        
        for (int i = 1;i <= n/2;i++){
            if (n%i == 0){
                System.out.print(i+", ");
            }
        }
        System.out.println(n+"]");
    }
}